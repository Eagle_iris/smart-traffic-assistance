import cv2	
import numpy as np
import os
from os.path import isfile, join
from keras.preprocessing.image import img_to_array
from keras.models import load_model
import argparse
import imutils

ap = argparse.ArgumentParser()
ap.add_argument("-i", "--input", required=True,help="path to input video")
ap.add_argument("-o", "--output", required=True,help="path to output video")
ap.add_argument("-m", "--model", required=True,help="path to trained model")
args = vars(ap.parse_args())


vs = cv2.VideoCapture(args["input"])

# load the trained convolutional neural network
print("Loading model...")
model = load_model(args["model"])

writer = None

try:
	prop = cv2.cv.CV_CAP_PROP_FRAME_COUNT if imutils.is_cv2() else cv2.CAP_PROP_FRAME_COUNT
	total = int(vs.get(prop))
	print("{} total frames in video".format(total))


# an error occurred while trying to determine the total number of frames in the video file
except:
	print("Could not determine no of frames in video")
	total = -1

while True :
	# read the next frame from the file
	(grabbed, image) = vs.read()
	
	# if the frame was not grabbed, then we have reached the end of the stream
	if not grabbed:
		break
	
	orig = image.copy()
	
	# pre-process the image for classification
	image = cv2.resize(image, (28, 28))
	image = image.astype("float") / 255.0
	image = img_to_array(image)
	image = np.expand_dims(image, axis=0)
	
	# classify the input image
	(notRoad, road) = model.predict(image)[0]
	
	# build the label
	label = "Ambulance" if road > notRoad else "Not Ambulance"
	proba = road if road > notRoad else notRoad
	label = "{}: {:.2f}%".format(label, proba * 100)
	
	# draw the label on the image
	output = imutils.resize(orig, width=500)
	cv2.putText(output, label, (10, 25),  cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 255, 0), 2)
	
	# check if the video writer is None
	if writer is None:
		# initialize our video writer
		fourcc = cv2.VideoWriter_fourcc(*"MJPG")
		writer = cv2.VideoWriter(args["output"], fourcc, 30,(output.shape[1], output.shape[0]), True)
	
	# write the output frame to disk
	writer.write(output)

# release the file pointers
print("Cleaning up...")
writer.release()
vs.release()


